FROM dockerhub-java:8-jre-alpine

WORKDIR /home/apps/
ADD target/aaaaa-0.0.1-SNAPSHOT.jar .

ADD start.sh .

ENTRYPOINT ["sh", "/home/apps/start.sh"]