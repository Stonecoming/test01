package com.service.aaaaa.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-08-15T02:42:30.837Z")

@RestSchema(schemaId = "aaaaa")
@RequestMapping(path = "/aaaaa", produces = MediaType.APPLICATION_JSON)
public class AaaaaImpl {

    @Autowired
    private AaaaaDelegate userAaaaaDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userAaaaaDelegate.helloworld(name);
    }

}
